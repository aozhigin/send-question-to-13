module.exports = {
    "parser": "@typescript-eslint/parser",
    extends: [
        "plugin:prettier/recommended",
        "prettier/@typescript-eslint",
        'eslint:recommended',
        "plugin:@typescript-eslint/recommended",
    ],
    'env': {
        'node': true,
        'commonjs': true,
        'es6': true
    },
    'globals': {
        'Atomics': 'readonly',
        'SharedArrayBuffer': 'readonly'
    },
    'parserOptions': {
        'ecmaVersion': 2018,
        'sourceType': "module"
    },
    'rules': {
        '@typescript-eslint/indent': [
            'error',
            4
        ],
        "@typescript-eslint/camelcase": 0,
        'linebreak-style': [
            'error',
            'unix'
        ],
    }
};