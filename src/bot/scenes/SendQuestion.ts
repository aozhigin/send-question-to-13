import { ChgkBotContext } from '../typings/context.h';
import { Scenes } from 'telegraf';
import { MAIN_SCENE_ID, SEND_QUESTION_SCENE_ID } from './constants';
import { isTextMessage } from './utils';
import { getId } from '../utils/getId';
import { logger } from '../../modules/logger/logger';

const MAX_QUESTION_LENGTH = 150;
const MAX_ANSWER_LENGTH = 75;

interface SendQuestionSceneState {
    question: string;
    answer: string;
    source: string;
}

export const sendQuestionScene = new Scenes.WizardScene<ChgkBotContext>(
    SEND_QUESTION_SCENE_ID,
    async context => {
        context.scene.state = {};
        await context.reply(`Какой вопрос?`);
        return context.wizard.next();
    },
    async context => {
        if (isTextMessage(context.message)) {
            const { text } = context.message;

            if (text.length > MAX_QUESTION_LENGTH) {
                return context.reply(
                    `Вопрос слишком длинный. Постарайтесь уложиться в ${MAX_QUESTION_LENGTH} символов.`,
                );
            }

            context.scene.state = { ...context.scene.state, question: text };
            await context.reply('Неплохо 🤔\nА какой ответ?');

            return context.wizard.next();
        } else {
            return context.reply('Мне нужен вопрос :)');
        }
    },
    async context => {
        if (isTextMessage(context.message)) {
            const { text } = context.message;

            if (text.length > MAX_ANSWER_LENGTH) {
                return context.reply(`Ответ слишком длинный. Постарайтесь уложиться в ${MAX_ANSWER_LENGTH} символов.`);
            }

            context.scene.state = { ...context.scene.state, answer: text };
            await context.reply('Вау!\nА откуда вы об этом узнали? Расскажите об источнике');

            return context.wizard.next();
        } else {
            context.reply('Чем хорош вопрос без ответа?');
        }
    },
    async context => {
        if (isTextMessage(context.message)) {
            const { text } = context.message;

            if (!text.length) {
                return context.reply(`Пожалуйста, укажите источник вашей информации`);
            }

            context.scene.state = { ...context.scene.state, source: text };

            try {
                const userId = await getId(context);

                await context.db.saveQuestion({
                    ...(context.scene.state as SendQuestionSceneState),
                    user_id: userId,
                });

                await context.replyWithMarkdown(
                    `Спасибо! Ваш вопрос сохранен.\n\nЯ отправлю его как только начнется следующий прямой эфир *"Что? Где? Когда?"* и пришлю вам уведомление.`,
                );
            } catch (error) {
                logger.error(error);
                await context.replyWithMarkdown(
                    `Oh, s**t! I'm sorry. 🌚 \n\nПроизошла ошибка во время сохранения вопроса.\n\nПопробуйте еще раз позднее.`,
                );
            }

            await context.scene.leave();

            return context.scene.enter(MAIN_SCENE_ID);
        } else {
            return context.reply(`Вы же знаете какой строгий Mr. Hook!\nОн не примет вопрос без источника!`);
        }
    },
);
