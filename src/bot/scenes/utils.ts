import { Message } from 'typegram';

export const isTextMessage = (message: any): message is Message.TextMessage =>
    !!message && typeof message.text === 'string';
