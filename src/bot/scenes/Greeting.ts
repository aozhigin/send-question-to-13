import { Scenes } from 'telegraf';
import emailValidator from 'email-validator';
import { GREETING_SCENE_ID, MAIN_SCENE_ID, stickerFileIds } from './constants';
import { isTextMessage } from './utils';
import { ChgkBotContext } from '../typings/context.h';
import { logger } from './../../modules/logger/logger';
import { getId } from '../utils/getId';

const MAX_NAME_LENGTH = 20;

const greetingMessage = `
Привет!

Я умею отправлять вопросы в 13 сектор *"Что? Где? Когда?"* ровно в начало прямого эфира.

Чтобы я мог отправить вопрос, мне нужно кое-что о вас знать:
 - имя и фамилию
 - город проживания
 - email
 - телефон

Давайте начнем с имени и фамилии. Представьтесь, пожалуйста.`;

const endSceneMessage = `
Супер!

Я сохранил ваши данные и укажу их при отправке вопроса.
`;

export const greetingScene = new Scenes.WizardScene<ChgkBotContext>(
    GREETING_SCENE_ID,
    async context => {
        const userId = await getId(context);

        logger.info({ chatId: userId }, `user enter scene: ${GREETING_SCENE_ID}`);

        let user = null;

        try {
            user = await context.db.getUser({ id: userId });
        } catch (error) {
            logger.error(error);
        }

        if (user) {
            await context.reply(`С возвращением, ${user.name}! 👋`);
            await context.scene.leave();

            return context.scene.enter(MAIN_SCENE_ID);
        }

        await context.replyWithSticker(stickerFileIds.SVOBODNY_WAVE_STRAW_HAT);
        await context.replyWithMarkdown(greetingMessage);

        context.scene.session.userInfo = {};
        context.wizard.next();
    },
    async context => {
        if (isTextMessage(context.message)) {
            const { text } = context.message;

            const name = text.trim().split(/\s+/);

            if (name.length !== 2) {
                context.reply('Что-то не пойму\nПожалуйста, пришлите имя и фамилию');
                return;
            }

            if (text.length > MAX_NAME_LENGTH) {
                context.reply(`Имя и фамилия слишком длинные.\nПостарайтесь уложиться в ${MAX_NAME_LENGTH} символов.`);
                return;
            }

            logger.debug({ text, message: context.message }, 'user.name');

            context.scene.session.userInfo = { ...context.scene.session.userInfo, name: name.join(' ') };
            await context.reply('Очень приятно!\nКак называется город откуда вы?');
            return context.wizard.next();
        } else {
            context.reply('Мне нужно ваше имя :)\nПришлите его в формате [Имя Фамилия] и продолжим');
            logger.debug(context.message, 'strange message');
        }
    },
    context => {
        if (isTextMessage(context.message)) {
            const { text } = context.message;
            const MAX_CITY_LENGTH = 20;

            if (text.length > MAX_CITY_LENGTH) {
                context.reply(
                    `Слишком длинное название города.\nПожалуйста, постарайтесь уложиться в ${MAX_CITY_LENGTH} символов.`,
                );
                return;
            }

            logger.debug({ text, message: context.message }, 'user.city');

            context.scene.session.userInfo = { ...context.scene.session.userInfo, city: text };
            context.reply('Здорово! Теперь скажите мне ваш email.');
            context.wizard.next();
        } else {
            context.reply('Мне нужен ваш город :)\nПришлите его название и сможем продолжить.');
            logger.debug(context.message, 'strange message');
        }
    },
    context => {
        if (isTextMessage(context.message)) {
            const { text } = context.message;

            if (!emailValidator.validate(text)) {
                context.reply(`Непохоже на email 🤔\nПопробуйте еще раз`);
                return;
            }

            logger.debug({ text, message: context.message }, 'user.email');

            context.scene.session.userInfo = { ...context.scene.session.userInfo, email: text };
            context.reply('Спасибо! И наконец ваш телефон.');
            context.wizard.next();
        } else {
            context.reply('Мне нужен ваш email :)\nПришлите его и сможем продолжить.');
            logger.debug(context.message, 'strange message');
        }
    },
    async context => {
        if (isTextMessage(context.message)) {
            const { text } = context.message;

            logger.debug({ text, message: context.message }, 'user.phone');

            context.scene.session.userInfo = { ...context.scene.session.userInfo, phone: text };

            await context.replyWithMarkdown(endSceneMessage);

            const { phone, email, name, city } = context.scene.session.userInfo;

            try {
                await context.db.saveUser({
                    id: await getId(context),
                    phone,
                    email,
                    name,
                    city,
                });
            } catch (error) {
                logger.error({ error }, 'user saving error');
            }

            await context.scene.leave();

            return context.scene.enter(MAIN_SCENE_ID);
        } else {
            context.reply('Мне нужен ваш телефон :)\nПришлите его и сможем продолжить.');
            logger.debug(context.message, 'strange message');
        }
    },
);
