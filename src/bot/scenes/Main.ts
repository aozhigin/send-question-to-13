import { logger } from '../../modules/logger/logger';
import { Markup, Scenes } from 'telegraf';
import { MAIN_SCENE_ID, SEND_QUESTION_SCENE_ID, stickerFileIds } from './constants';
import { getId } from '../utils/getId';

const enum MainActions {
    sendQuestion = 'Отправить вопрос',
}

export const mainScene = new Scenes.BaseScene<Scenes.SceneContext>(MAIN_SCENE_ID);

mainScene.enter(async context => {
    logger.info({ chatId: await getId(context) }, `user enter scene: ${MAIN_SCENE_ID}`);

    return context.reply(
        'Можете выбрать, что сделать дальше.',
        Markup.inlineKeyboard([Markup.button.callback(MainActions.sendQuestion, MainActions.sendQuestion)]),
    );
});

mainScene.action(MainActions.sendQuestion, async context => {
    await context.replyWithSticker(stickerFileIds.SVOBODNY_OK_MONITOR, Markup.removeKeyboard());
    return context.scene.enter(SEND_QUESTION_SCENE_ID);
});

mainScene.use(async (context, next) => {
    await context.reply('Не понимаю', Markup.removeKeyboard());
    next();
});
