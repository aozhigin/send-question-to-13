import { Database } from 'sqlite3';

import { Logger } from 'pino';
import { User, Question } from '../typings/db.h';

interface DBServiceOptions {
    db: Database;
    logger: Logger;
}

export class DBService {
    private db: Database;
    private logger: Logger;

    constructor(options: DBServiceOptions) {
        this.db = options.db;
        this.logger = options.logger;
    }

    getUser({ id }: Pick<User, 'id'>): Promise<User | undefined> {
        return new Promise((resolve, reject) => {
            this.db.get(`select * from users where id = ?`, [id], (error, row) => {
                if (error) {
                    this.logger.error(error, `dberror: get user ${id}`);
                    reject(error);

                    return;
                }
                resolve(row);
            });
        });
    }

    saveUser({ city, name, phone, email, id }: User): Promise<void> {
        return new Promise((resolve, reject) => {
            this.db.run(
                `insert into users 
                (city, name, phone, email, id)
                values (?, ?, ?, ?, ?)`,
                [city, name, phone, email, id],

                error => {
                    if (error) {
                        this.logger.error(error, 'dberror: saving user');
                        reject(error);

                        return;
                    }

                    this.logger.info({ id }, 'user saved');

                    resolve();
                },
            );
        });
    }

    saveQuestion({ source, answer, question, user_id }: Omit<Question, 'id'>): Promise<void> {
        return new Promise((resolve, reject) => {
            this.db.run(
                `insert into questions 
                (source, answer, question, user_id)
                values (?, ?, ?, ?)`,
                [source, answer, question, user_id],

                error => {
                    if (error) {
                        this.logger.error(error, 'dberror: saving question');
                        reject(error);

                        return;
                    }

                    this.logger.info(`question from ${user_id} saved`);

                    resolve();
                },
            );
        });
    }
}
