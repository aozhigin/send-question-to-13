import sqlite3 from 'sqlite3';
import path from 'path';

import { DBService } from './DBService';
import { logger } from '../../modules/logger/logger';

if (process.env.NODE_ENV == 'development') {
    sqlite3.verbose();
}

export function initDatabase(): Promise<{ db: DBService }> {
    const dbFile = `./chgk${process.env.NODE_ENV == 'development' ? '_dev' : ''}.db`;

    return new Promise((resolve, reject) => {
        const db = new sqlite3.Database(path.resolve(process.env.DATABASE_FOLDER, dbFile), err => {
            if (err) {
                reject(err);
                return;
            }

            logger.info({ dbFile }, 'connected to db');

            resolve({ db: new DBService({ logger, db }) });
        });
    });
}
