import { Context } from 'telegraf';

export async function getId(ctx: Context): Promise<string | undefined> {
    const fromId = ctx.from?.id;
    const chatId = ctx.chat?.id;
    if (fromId == null || chatId == null) {
        return undefined;
    }
    return `${fromId}:${chatId}`;
}
