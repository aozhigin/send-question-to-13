require('dotenv').config();

import { greetingScene } from './scenes/Greeting';
import { mainScene } from './scenes/Main';
import { sendQuestionScene } from './scenes/SendQuestion';
import { GREETING_SCENE_ID } from './scenes/constants';
import { getId } from './utils/getId';
import { Telegraf, Scenes, session } from 'telegraf';

import { initDatabase } from './database';
import { ChgkBotContext } from './typings/context.h';
import { logger } from '../modules/logger/logger';
import { createLoggerMiddleware } from '../modules/logger/telegrafLogger';

const bot = new Telegraf<ChgkBotContext>(process.env.TELEGRAM_BOT_TOKEN);

const stage = new Scenes.Stage<Scenes.SceneContext>([greetingScene, mainScene, sendQuestionScene]);

initDatabase()
    .then(({ db }) => {
        /**
         * мидлвара с прокидыванием бд идет первой, чтобы быть доступной во всех обработчиках
         */
        bot.use((context, next) => {
            context.db = db;
            return next();
        });

        if (process.env.NODE_ENV == 'development') {
            bot.use(createLoggerMiddleware(logger));
        }
        bot.use(session({ getSessionKey: getId }));
        bot.use(stage.middleware());

        bot.start(context => {
            return context.scene.enter(GREETING_SCENE_ID);
        });

        return bot.launch();
    })
    .then(() => {
        logger.info('Bot is launched.');
    })
    .catch(error => {
        logger.error({ error }, 'Error while starting bot. Exiting...');
        process.exit(1);
    });

// Enable graceful stop
process.once('SIGINT', () => bot.stop('SIGINT'));
process.once('SIGTERM', () => bot.stop('SIGTERM'));
