export interface User {
    city: string;
    name: string;
    phone: string;
    email: string;
    id: string;
}

export interface Question {
    source: string;
    answer: string;
    question: string;
    user_id: string;
    id: string;
}
