import { DBService } from 'bot/database/DBService';
import { Scenes, Context } from 'telegraf';

export interface UserInfo {
    name: string;
    phone: string;
    email: string;
    city: string;
}

export interface ChgkBotWizardSession extends Scenes.WizardSessionData {
    userInfo: Partial<UserInfo>;
}

export interface ChgkBotSession extends Scenes.WizardSession<ChgkBotWizardSession> {
    id: string;
}

export interface ChgkBotContext extends Context {
    db: DBService;

    session: ChgkBotSession;
    scene: Scenes.SceneContextScene<ChgkBotContext, ChgkBotWizardSession>;
    wizard: Scenes.WizardContextWizard<ChgkBotContext>;
}
