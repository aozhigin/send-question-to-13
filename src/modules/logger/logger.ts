import pino from 'pino';

let level = process.env.DEBUG_LEVEL;

if (!pino.levels.values[level]) {
    level = 'info';
}

const logger = pino({ level, prettyPrint: process.env.NODE_ENV === 'development' ? { colorize: true } : null });

if (level !== process.env.DEBUG_LEVEL && process.env.DEBUG_LEVEL) {
    logger.error({ value: process.env.DEBUG_LEVEL }, 'unknown DEBUG_LEVEL value');
}

export { logger };
