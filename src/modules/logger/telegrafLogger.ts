import { Logger } from 'pino';
import { Context, MiddlewareFn, Telegraf } from 'telegraf';

export const createLoggerMiddleware = (logger: Logger): MiddlewareFn<Context> => (ctx, next) => {
    logger.debug(ctx.update);
    next();
};
