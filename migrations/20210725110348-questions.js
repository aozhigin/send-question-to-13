'use strict';

const { dataType } = require('db-migrate');

const TABLE_NAME = 'questions';

exports.up = function(db) {
    return db.createTable(TABLE_NAME, {
        source: dataType.STRING,
        answer: dataType.STRING,
        question: dataType.STRING,
        user_id: dataType.STRING,
        id: {
            type: dataType.INTEGER,
            unsigned: true,
            notNull: true,
            primaryKey: true,
            autoIncrement: true,
        },
    });
};

exports.down = function(db) {
    return db.dropTable(TABLE_NAME);
};

exports._meta = {
    version: 1,
};
