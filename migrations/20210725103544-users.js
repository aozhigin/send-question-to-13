'use strict';

const { dataType } = require('db-migrate');

const TABLE_NAME = 'users';

exports.up = function(db) {
    return db.createTable(TABLE_NAME, {
        city: dataType.STRING,
        name: dataType.STRING,
        phone: dataType.STRING,
        email: dataType.STRING,
        id: {
            type: dataType.STRING,
            unsigned: true,
            notNull: true,
            primaryKey: true,
        },
    });
};

exports.down = function(db) {
    return db.dropTable(TABLE_NAME);
};

exports._meta = {
    version: 1,
};
